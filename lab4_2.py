import data_labs

# Блок ввода данных
while True:
    choose = input('Какую функцию следует выполнить? (G || F ||Y : ')
    variable_data = list(map(float, input('По порядку введите x min, x max, a и шаг: ').split()))
    data_function = []

    x = variable_data[0]
    while x < variable_data[1]:
        try:
            # Блок расчёта данных G
            if choose == 'G':
                try:
                    print(f'x = {x:.5f} и f(x) = {data_labs.g_function(x, variable_data[2]):.5f}')
                    data_function.append(data_labs.g_function(x, variable_data[2]))
                except ZeroDivisionError:
                    print(f'Ошибка расчёта значения G при х = {variable_data[2]}')
                    data_function.append(None)

            # Блок расчёта и вывода данных F
            elif choose == 'F':
                try:
                    print(f'x = {x:.5f} и f(x) = {data_labs.f_function(x, variable_data[2]):.5f}')
                    data_function.append(data_labs.f_function(x, variable_data[2]))
                except OverflowError:
                    print(f'Ошибка расчёта значения F при х = {variable_data[2]}')
                    data_function.append(None)

            # Блок расчёта и вывода данных Y
            elif choose == 'Y':
                try:
                    print(f'x = {x:.5f} и f(x) = {data_labs.y_function(x, variable_data[2]):.5f}')
                    data_function.append(data_labs.y_function(x, variable_data[2]))
                except ValueError or OverflowError:
                    print(f'Ошибка расчёта значения Y при х = {variable_data[2]}')
                    data_function.append(None)
            x += variable_data[3]

            # Блок ошибки ввода значения value
        except KeyboardInterrupt:
            print('Я упавсь')

    if len(data_function) == 1:
        if None in data_function:
            print('Значения данных инкорректны')
        else:
            print(f'Min  = max x  {list(data_function)[0]:.5f}')
    elif None in data_function:
        data_function.pop(None)
        print(f'Min1 = {sorted(data_function)[0]} и Max1 = {sorted(data_function)[-1]}')
    else:
        print(f'Min2 = {sorted(data_function)[0]} и Max2 = {sorted(data_function)[-1]}')

    # Завершение/продолжение программы
    exit_choice = input("Начать выполнение программы снова? - (Д/Н)")
    if 'YyNnДдНн'.find(exit_choice) == -1:
        print('Ошибка выбора.')
    elif 'YyДд'.find(exit_choice) >= 0:
        pass
    elif 'NnНн'.find(exit_choice) >= 0:
        raise SystemExit()