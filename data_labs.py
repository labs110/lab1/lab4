import math


def g_function(x, a):
    return (6 * (4 * a ** 2 - 12 * a * x + 5 * x ** 2)) / (9 * a ** 2 + 30 * a * x + 16 * x ** 2)


def f_function(x, a):
    return 5 ** (a ** 2 - 5 * a * x + 4 * x ** 2)


def y_function(x, a):
    return math.log(-a ** 2 - 7 * a * x + 8 * x ** 2 + 1) / math.log(2)
